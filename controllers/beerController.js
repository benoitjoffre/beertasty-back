let Beer = require('../models/beerModel');
let aqp = require('api-query-params');
const addBeer = (req, res) => {
    let beer = new Beer();
    beer.beername = req.body.beername;
    beer.breweryname = req.body.breweryname;
    beer.description = req.body.description;
    beer.alcohol = req.body.alcohol;
    beer.aromatic = req.body.aromatic;
    beer.bitter = req.body.bitter;
    if(req.body.flavor) {beer.flavor = req.body.flavor};

    beer.save((err) => {
        if(err) {
            res.json(err);
        } else {
            res.json({
                message: 'La nouvelle bière à été ajoutée',
                data: beer
            })
        }
       
    })
}

const searchBeer = async (req, res) => {
    console.log(req.query)
    const { filter } = aqp(req.query);
    
    if(filter) {
        const result = await Beer.find(filter)
        res.json(result)
    } else {
        const result = await Beer.find()
        res.json(result)
    }

}

const getBeer = async (req, res) => {
    let result = await Beer.find({_id: req.params.id})
    res.json(result)
}

module.exports = {addBeer, searchBeer, getBeer}