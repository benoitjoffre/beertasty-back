
let router = require('express').Router();

router.get('/search', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to BeerTasty the beerTastor crafted with love!'
    });
});
var beerController = require('./controllers/beerController');

router.route('/beers')
    .get(beerController.searchBeer)
    .post(beerController.addBeer)
router.route('/beers/:id')
    .get(beerController.getBeer)

module.exports = router;