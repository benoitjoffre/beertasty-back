let express = require('express')

let apiRoutes = require("./api-routes")
let mongoose = require('mongoose');
var cors = require('cors')
let app = express();

var port = process.env.PORT || 3000;

app.use(cors())
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api', apiRoutes)

const uri = 'mongodb+srv://benoit:pass4dev@cluster0.mga3g.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

mongoose.connect(uri);

app.listen(port, function () {
     console.log("Running BeerTastyAPI on port " + port);
});