var mongoose = require('mongoose');

var beerSchema = mongoose.Schema({
    beername: {
        type: String,
        
        unique: true
    },
    breweryname: {
        type: String,
        
    },
    description: {
        type: String,
        
    },
    alcohol: {
        type: Number,
        
    },
    aromatic: {
        type: Number,
        
    },
    bitter: {
        type: Number
    },
    flavor: {
        type: [String],
        default: undefined,
        
    }
});
// Export Contact model
var Beer = module.exports = mongoose.model('beer', beerSchema);
